package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/* Autor: Diana Pawłowska
 * Klasa otwiera nowe okno JFrame i wyświetla JLabel z napisem pobieranym z 
 * tablicy tłumaczeń. Napis dotyczy tego kto jest autorem programu.
 * */

public class Authors extends JFrame {

	private static final long serialVersionUID = 8937720397459219989L;

	public Authors() throws HeadlessException {
		super(Language.translation[20]);
		setLayout(new BorderLayout());
		setBounds(800, 300, 200, 200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);

		JLabel authorsLabel = new JLabel(Language.translation[24]);
		add(authorsLabel, BorderLayout.CENTER);

		JButton okButton = new JButton("Ok");
		add(okButton, BorderLayout.SOUTH);

		ActionListener closeListener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				dispose();

			}
		};
		okButton.addActionListener(closeListener);
	}

}

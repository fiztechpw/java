package pl.edu.pw.fizyka.pojava.FilDia;

/* Autor: Diana Pawłowska
 *Klasa Balls zawiera opis obiektów typu Kulka. Dodatkowo zawiera metody zwracania pól prywatnych klasy,
 *oraz konstruktor domyślny i główny.
 * */

public class Balls {

	private double x, y;
	private double velocity;
	private double radius;

	public Balls() {
		// TODO Auto-generated constructor stub
		x = y = velocity = radius = 0;
	}

	public Balls(double a, double b, double c, double d) {
		// TODO Auto-generated constructor stub
		x = a;
		y = b;
		velocity = c;
		radius = d;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getVelocity() {
		return velocity;
	}

	public void setVelocity(double velocity) {
		this.velocity = velocity;
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

}

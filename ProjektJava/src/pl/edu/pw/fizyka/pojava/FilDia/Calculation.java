package pl.edu.pw.fizyka.pojava.FilDia;

import java.util.Random;

/* Autor: Filip Oleszczuk
 * Klasa typu Thread do obliczeń.
 * */

public class Calculation extends Thread {
	DrawingWindow drawingWindow;
	static int movingBalls = 1;
	public static boolean doAnimation = false;
	public static int animationSpeed = 10;
	public static int n = 0;
	// Number of balls that didnt pass any cover
	public static int n1 = 0;
	// Number of balls that passed 1 cover
	public static int n2 = 0;
	// Number of balls that passed 2 covers
	public static int n3 = 0;
	// Number of balls that passed 3 covers
	public static int N = DrawingWindow.numberOfBalls;
	// Number of all balls generated
	public static int temp = 0, temp1 = 0, temp2 = 0, temp3 = 0;

	public void run() {

		do {

			while (movingBalls != DrawingWindow.numberOfBalls && doAnimation) {

				for (int ii = 0; ii < movingBalls; ii++) {
					DrawingWindow.balls[ii].setX(DrawingWindow.balls[ii].getX()
							+ DrawingWindow.balls[ii].getVelocity());

					if (DrawingWindow.balls[ii].getX() > 560) {
						DrawingWindow.balls[ii].setX(10);

					}

					switch (Parameters.numberOfCovers) {
					case 3: {
						if (DrawingWindow.balls[ii].getX() >= 152
								&& DrawingWindow.balls[ii].getX() < 152 + 29 + 2 * Parameters.cmCovers[0]) {

							if (probabilityOfParticleAnnihilation(0)) {
								DrawingWindow.balls[ii].setX(10);
								N++;
								n++;
							}

						}

						if (DrawingWindow.balls[ii].getX() > 307
								&& DrawingWindow.balls[ii].getX() < 307 + 29 + 2 * Parameters.cmCovers[1]) {

							if (probabilityOfParticleAnnihilation(1)) {
								DrawingWindow.balls[ii].setX(10);

								N++;
								n1++;
							}
						}

						if (DrawingWindow.balls[ii].getX() > 462
								&& DrawingWindow.balls[ii].getX() < 462 + 29 + 2 * Parameters.cmCovers[2]) {

							if (probabilityOfParticleAnnihilation(2)) {
								DrawingWindow.balls[ii].setX(10);

								N++;
								n2++;
							}
						}
						break;
					}
					case 2: {
						if (DrawingWindow.balls[ii].getX() >= 152
								&& DrawingWindow.balls[ii].getX() < 152 + 29 + 2 * Parameters.cmCovers[0]) {

							if (probabilityOfParticleAnnihilation(0)) {
								DrawingWindow.balls[ii].setX(10);
								N++;
								n++;
							}

						}

						if (DrawingWindow.balls[ii].getX() > 307
								&& DrawingWindow.balls[ii].getX() < 307 + 29 + 2 * Parameters.cmCovers[1]) {

							if (probabilityOfParticleAnnihilation(1)) {
								DrawingWindow.balls[ii].setX(10);

								N++;
								n1++;
							}
						}
						break;
					}
					case 1: {
						if (DrawingWindow.balls[ii].getX() >= 152
								&& DrawingWindow.balls[ii].getX() < 152 + 29 + 2 * Parameters.cmCovers[0]) {

							if (probabilityOfParticleAnnihilation(0)) {
								DrawingWindow.balls[ii].setX(10);
								N++;
								n++;
							}

						}

						break;
					}

					}

				}

				while (temp != n) {
					Histogram.dataset.addObservation(0);
					temp++;
				}
				while (temp1 != n1) {
					Histogram.dataset.addObservation(1);
					temp1++;
				}
				while (temp2 != n2) {
					Histogram.dataset.addObservation(2);
					temp2++;
				}
				while (temp3 != n3) {
					Histogram.dataset.addObservation(3);
					temp3++;
				}

				try {
					sleep(animationSpeed);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				movingBalls++;
				drawingWindow.repaint();
			}

			for (int ii = 0; ii < DrawingWindow.numberOfBalls; ii++) {
				DrawingWindow.balls[ii].setX(DrawingWindow.balls[ii].getX()
						+ DrawingWindow.balls[ii].getVelocity());

				if (DrawingWindow.balls[ii].getX() > 560) {
					DrawingWindow.balls[ii].setX(10);

					n3++;
				}

				switch (Parameters.numberOfCovers) {
				case 3: {
					if (DrawingWindow.balls[ii].getX() > 152
							&& DrawingWindow.balls[ii].getX() < 152 + 29 + 2 * Parameters.cmCovers[0]) {

						if (probabilityOfParticleAnnihilation(0)) {
							DrawingWindow.balls[ii].setX(10);

							N++;
							n++;
						}
					}

					if (DrawingWindow.balls[ii].getX() > 307
							&& DrawingWindow.balls[ii].getX() < 307 + 29 + 2 * Parameters.cmCovers[1]) {

						if (probabilityOfParticleAnnihilation(1)) {
							DrawingWindow.balls[ii].setX(10);

							N++;
							n1++;
						}
					}

					if (DrawingWindow.balls[ii].getX() > 462
							&& DrawingWindow.balls[ii].getX() < 462 + 29 + 2 * Parameters.cmCovers[2]) {

						if (probabilityOfParticleAnnihilation(2)) {
							DrawingWindow.balls[ii].setX(10);

							N++;
							n2++;
						}
					}

					break;
				}
				case 2: {
					if (DrawingWindow.balls[ii].getX() > 152
							&& DrawingWindow.balls[ii].getX() < 152 + 29 + 2 * Parameters.cmCovers[0]) {

						if (probabilityOfParticleAnnihilation(0)) {
							DrawingWindow.balls[ii].setX(10);

							N++;
							n++;
						}
					}

					if (DrawingWindow.balls[ii].getX() > 307
							&& DrawingWindow.balls[ii].getX() < 307 + 29 + 2 * Parameters.cmCovers[1]) {

						if (probabilityOfParticleAnnihilation(1)) {
							DrawingWindow.balls[ii].setX(10);

							N++;
							n1++;
						}
					}
					break;
				}
				case 1: {
					if (DrawingWindow.balls[ii].getX() >= 152
							&& DrawingWindow.balls[ii].getX() < 152 + 29 + 2 * Parameters.cmCovers[0]) {

						if (probabilityOfParticleAnnihilation(0)) {
							DrawingWindow.balls[ii].setX(10);

							N++;
							n++;
						}
					}

					break;
				}

				}

			}

			while (temp != n) {
				Histogram.dataset.addObservation(0);
				temp++;
			}
			while (temp1 != n1) {
				Histogram.dataset.addObservation(1);
				temp1++;
			}
			while (temp2 != n2) {
				Histogram.dataset.addObservation(2);
				temp2++;
			}
			while (temp3 != n3) {
				Histogram.dataset.addObservation(3);
				temp3++;
			}

			try {
				sleep(animationSpeed);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}
			drawingWindow.repaint();
		} while (doAnimation == true);

	}

	public Calculation() {

	}

	public Calculation(DrawingWindow drawingWindowMain) {
		drawingWindow = drawingWindowMain;
	}

	public boolean probabilityOfParticleAnnihilation(int coverNumber) {
		boolean ballExistOrNot = true;
		Random randomNumber = new Random();
		double probability = 0;
		if (Main.monoenergetic == true) {
			switch (Main.radiationEnergy) {
			case 5:
				if (Parameters.whatCovers[coverNumber] == 0) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.0066 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.0066 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.0066 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.0066 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.0066 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.0066 * 3.37;
					}

				}
				if (Parameters.whatCovers[coverNumber] == 1) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.049 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.049 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.049 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.049 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.049 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.049 * 3.37;
					}

				}
				break;
			case 10:
				if (Parameters.whatCovers[coverNumber] == 0) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.0052 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.0052 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.0052 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.0052 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.0052 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.0052 * 3.37;
					}

				}
				if (Parameters.whatCovers[coverNumber] == 1) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.056 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.056 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.056 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.056 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.056 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.056 * 3.37;
					}

				}

				break;
			case 15:
				if (Parameters.whatCovers[coverNumber] == 0) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.0048 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.0048 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.0048 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.0048 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.0048 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.0048 * 3.37;
					}

				}
				if (Parameters.whatCovers[coverNumber] == 1) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.064 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.064 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.064 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.06456 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.064 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.064 * 3.37;
					}

				}

				break;
			case 20:
				if (Parameters.whatCovers[coverNumber] == 0) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.0046 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.0046 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.0046 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.0046 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.0046 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.0046 * 3.37;
					}

				}
				if (Parameters.whatCovers[coverNumber] == 1) {
					if (Parameters.cmCovers[coverNumber] == 5) {
						probability = 0.07 * 1.28;
					}

					if (Parameters.cmCovers[coverNumber] == 10) {
						probability = 0.07 * 2.04;
					}

					if (Parameters.cmCovers[coverNumber] == 15) {
						probability = 0.07 * 2.54;
					}

					if (Parameters.cmCovers[coverNumber] == 20) {
						probability = 0.07 * 2.89;
					}

					if (Parameters.cmCovers[coverNumber] == 25) {
						probability = 0.07 * 3.16;
					}

					if (Parameters.cmCovers[coverNumber] == 30) {
						probability = 0.07 * 3.37;
					}

				}

				break;

			}

			if (randomNumber.nextDouble() * 1 < probability)
				ballExistOrNot = true;
			else
				ballExistOrNot = false;

		}
		if (Main.monoenergetic == false) {
			String temp = Main.radiationSource;
			double temp2 = randomNumber.nextDouble() * 100;
			if (temp == Language.translation[7]) {
				if (temp2 < 32) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0046 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0046 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0046 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.0046 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0046 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0046 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.069 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.069 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.069 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.069 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.069 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.069 * 3.37;
						}

					}
				}

				if (temp2 >= 32 && temp2 <= 67) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0033 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0033 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0033 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.0033 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0033 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0033 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.054 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.054 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.054 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.054 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.054 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.054 * 3.37;
						}

					}
				}

				if (temp2 >= 67) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0023 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0023 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0023 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.0023 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0023 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0023 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.088 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.088 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.088 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.088 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.088 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.088 * 3.37;
						}

					}
				}
			}
			if (temp == Language.translation[8]) {
				if (temp2 < 22) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0062 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0062 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0062 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.0062 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0062 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0062 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.045 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.045 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.045 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.045 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.045 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.045 * 3.37;
						}

					}
				}

				if (temp2 >= 22 && temp2 < 35) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0039 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0039 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0039 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.003952 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0039 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0039 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.058 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.058 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.058 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.058 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.058 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.058 * 3.37;
						}

					}
				}

				if (temp2 >= 35 && temp2 < 64) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0052 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0052 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0052 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.0052 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0052 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0052 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.056 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.056 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.056 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.056 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.056 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.056 * 3.37;
						}

					}
				}

				if (temp2 >= 64) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.0058 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.0058 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.0058 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.0058 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.0058 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.0058 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.06 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.06 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.06 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.06 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.06 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.06 * 3.37;
						}

					}
				}

			}

			if (temp == Language.translation[9]) {
				if (temp2 < 14) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.027 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.027 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.027 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.027 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.027 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.027 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 1.33 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 1.33 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 1.33 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 1.33 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 1.33 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 1.33 * 3.37;
						}

					}
				}

				if (temp2 >= 14 && temp2 <= 24) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.03 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.03 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.03 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.03 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.03 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.03 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 1.4 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 1.4 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 1.4 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 1.4 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 1.4 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 1.4 * 3.37;
						}

					}
				}

				if (temp2 >= 24 && temp2 <= 59) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.040 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.044 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.044 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.044 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.044 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.044 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 1.56 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 1.56 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 1.56 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 1.56 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 1.56 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 1.56 * 3.37;
						}

					}
				}

				if (temp2 >= 59 && temp2 <= 77) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.012 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.012 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.012 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.012 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.012 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.012 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 1.01 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 1.01 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 1.01 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 1.01 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 1.01 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 1.01 * 3.37;
						}

					}
				}

				if (temp2 > 77) {
					if (Parameters.whatCovers[coverNumber] == 0) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 0.013 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 0.013 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 0.013 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 0.013 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 0.013 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 0.013 * 3.37;
						}

					}
					if (Parameters.whatCovers[coverNumber] == 1) {
						if (Parameters.cmCovers[coverNumber] == 5) {
							probability = 1.23 * 1.28;
						}

						if (Parameters.cmCovers[coverNumber] == 10) {
							probability = 1.23 * 2.04;
						}

						if (Parameters.cmCovers[coverNumber] == 15) {
							probability = 1.23 * 2.54;
						}

						if (Parameters.cmCovers[coverNumber] == 20) {
							probability = 1.23 * 2.89;
						}

						if (Parameters.cmCovers[coverNumber] == 25) {
							probability = 1.23 * 3.16;
						}

						if (Parameters.cmCovers[coverNumber] == 30) {
							probability = 1.23 * 3.37;
						}

					}
				}
			}

			if (randomNumber.nextDouble() * 1 < probability)
				ballExistOrNot = true;
			else
				ballExistOrNot = false;

		}
		return ballExistOrNot;
	}

}

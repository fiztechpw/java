package pl.edu.pw.fizyka.pojava.FilDia;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/* Autor: Diana Pawłowska
 * Klasa rysująca wykresy teoretyczne.
 *
 * */

public class Chart extends JPanel {

	public static XYSeries dataSet = new XYSeries(Language.translation[46]);
	public static JFreeChart lineGraph;
	public static double fluxStart = 0;
	public static double currentFlux = 0;

	private static final long serialVersionUID = 8582587439869049160L;

	public Chart() {
		XYSeriesCollection xySeriesCollection = new XYSeriesCollection();


		
		XYDataset xyDataset = xySeriesCollection;
		xySeriesCollection.addSeries(dataSet);

		lineGraph = ChartFactory.createXYLineChart(Language.translation[45], // Title
				Language.translation[48], // X-Axis label
				Language.translation[47], // Y-Axis label
				xyDataset, // Dataset
				PlotOrientation.VERTICAL, // Plot orientation
				true, // show legend
				true, // Show tooltips
				false // url show
				);
		ChartPanel chartPanel = new ChartPanel(lineGraph);
		add(chartPanel);
		revalidate();

	}

}

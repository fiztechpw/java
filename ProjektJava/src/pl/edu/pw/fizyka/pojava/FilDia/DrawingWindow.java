package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

/* Autor: Filip Oleszczuk
 *Klasa dziedzicząca po JPanelu, w oknie głównym ma swoje miejsce, w którym rysuje kulki, emiter, boya,
 *przesłony. Jeśli ilość cząsteczek, które dotrą do boya jest większa niż 1000 zmienni sie on w boya 2.
 *Konstruktor klasy wywołuje funkcje fillBalls(), która generuje mi tablice obiektów typu Kulka, która zapełniam
 *wartościami randomowymi x i y oraz prędkośćia i promieniem kulku. Następnie wszystko rysuje w metodzie paint().
 *
 * */

public class DrawingWindow extends JPanel {
	public static int numberOfBalls = 300;

	private static final long serialVersionUID = 1L;
	public static Balls balls[] = new Balls[numberOfBalls];

	private Image boyOnAnimation1, boyOnAnimation2;

	public DrawingWindow() {
		boyOnAnimation1 = new ImageIcon(this.getClass().getResource("boy.jpg"))
				.getImage();
		boyOnAnimation2 = new ImageIcon(this.getClass().getResource(
				"radiation.jpg")).getImage();
		fillBalls();
	}

	public static void fillBalls() {
		if (Main.radiationSource == null) {
			Main.radiationSource = Language.translation[7];
		}
		Random randomGenerator = new Random();
		for (int ii = 0; ii < numberOfBalls; ii++) {
			balls[ii] = new Balls(randomGenerator.nextDouble() * 80,
					randomGenerator.nextDouble() * 80 + 155, 1, 10);

		}

	}

	public void paint(Graphics gg) {
		super.paint(gg);

		Graphics2D g = (Graphics2D) gg;
		if (Calculation.n3 < 1000)
			g.drawImage(boyOnAnimation1, 570, 130, null);
		else
			g.drawImage(boyOnAnimation2, 570, 130, null);
		g.setColor(Color.BLACK);
		g.drawRect(0, 150, 100, 100);

		for (int ii = 0; ii < Parameters.numberOfCovers; ii++) {
			if (Parameters.whatCovers[ii] == 0) {
				g.setColor(Color.LIGHT_GRAY);
				g.fillRect(152 + (155 * ii), 115,
						29 + 2 * Parameters.cmCovers[ii], 170);

			}
			if (Parameters.whatCovers[ii] == 1) {
				g.setColor(Color.darkGray);
				g.fillRect(152 + (155 * ii), 115,
						29 + 2 * Parameters.cmCovers[ii], 170);
			}

		}

		g.setColor(Color.RED);
		for (int ii = 0; ii < numberOfBalls; ii++) {
			Ellipse2D.Double shape = new Ellipse2D.Double(balls[ii].getX(),
					balls[ii].getY(), balls[ii].getRadius(),
					balls[ii].getRadius());
			g.fill(shape);

		}

	}

}

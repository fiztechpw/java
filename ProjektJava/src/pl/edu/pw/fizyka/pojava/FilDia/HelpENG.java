package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/* Autor: Diana Pawłowska
 * Klasa otwiera nowe okno JFrame i wyświetla JLabel z napisem.
 * */


public class HelpENG extends JFrame {

	private static final long serialVersionUID = 1768126046847154224L;

	public HelpENG() throws HeadlessException {
		super("Help");
		setLayout(new BorderLayout());
		setBounds(800, 300, 400, 650);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);

		JLabel authorsLabel = new JLabel(
				"<html><tab>The most common questions: <br><br>"
						+ "1. What is gamma radiation? <br> "
						+ "Gamma radiation is a high energy form of electromagnetic radiation. "
						+ "This radiation is produced by nuclear transitions, collisions of nuclei or subatomic particles.<br>"
						+ "<br>2. What happens to the gamma beam in a material medium? <br>"
						+ "Gamma-ray beam passing through the center of the material weakened."
						+ "It can be shown in an experimental way, that weakness is exponentially dependent on the thickness of the absorbent.<br>"
						+ "<br>3. What causes the weakening of gamma quanta? <br>"
						+ "Weakness is caused by three processes: "
						+ "photoelectric effect, Compton scattering and phenomenon of electron positron pair creation. <br>"
						+ "<br>4.What is a factor u?<br>"
						+ "It is a factor weakening of gamma radiation, usually served in a unit 1/mm.<br> "
						+ "<br>5. What determines the value of the coefficient of weakness?<br>"
						+ "It depends on the choice of material from which we will build the aperture and the radiation energy.<br> "
						+ "<br>6. Gamma radiation can be used for sterilization of medical devices, as well as food products."
						+ "Also applies to radiotherapy, industry and science, for example measuring the thickness of hot steel plates.</html>");
		add(authorsLabel, BorderLayout.CENTER);

		JButton okButton = new JButton("Ok");
		add(okButton, BorderLayout.SOUTH);

		// LISTENERY
		ActionListener closeListener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				dispose();

			}
		};
		okButton.addActionListener(closeListener);
	}
}
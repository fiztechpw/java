package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.BorderLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/* Autor: Diana Pawłowska
 * Klasa otwiera nowe okno JFrame i wyświetla JLabel z napisem.
 * */

public class HelpPL extends JFrame {

	private static final long serialVersionUID = 8937720397459219989L;

	public HelpPL() throws HeadlessException {
		super("Pomoc");
		setLayout(new BorderLayout());
		setBounds(800, 300, 400, 650);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);

		JLabel authorsLabel = new JLabel(
				"<html><tab>Najczęściej zadawane przez użytkowników pytania: <br><br>"
						+ "1. Czym jest promieniowanie gamma? <br> "
						+ "Promieniowanie gamma jest wysokoenergetyczną formą promieniowania elektromagnetycznego. "
						+ "Promieniowanie to wytwarzane jest w wyniku przemian jądrowych, zderzeń jąder bądź cząsteczek subatomowych.<br>"
						+ "<br>2. Co dzieje się z wiązką gamma w ośrodku materialnym? <br>"
						+ "Wiązka promieniowania gamma przechodząc przez ośrodek materialny ulega osłabieniu.<br>"
						+ "Można udowodnić w sposób eksperymentalny, że osłabienie to zależy wykładniczo od grubości absorbentu.<br>"
						+ "<br>3. Co powoduje osłabienie kwantów gamma? <br>"
						+ "Osłabienie jest spowodowane trzema procesami: "
						+ "efektem fotoelektrycznym, rozpraszaniem komptonowskim i zjawiskiem tworzenia par elektron - pozyton. <br>"
						+ "<br>4. Czym jest współczynnik u?<br>"
						+ "Jest to wspólczynnik osłabienia promieniowania gamma, podawany zazwyczaj w jednostce 1/mm.<br> "
						+ "<br>5. Od czego zależy wartość współczynnika osłabienia?<br>"
						+ "Zależy ona od wyboru materiału z jakiego zbudujemy przesłony oraz od energii promieniowania.<br> "
						+ "<br>6. Promieniowanie gamma może służyć do sterylizacji sprzętu medycznego, jak również produktów spożywczych."
						+ "Ponadto ma zastosowanie w radioterapii, przemyśle i nauce np. do pomiaru grubości gorących blach stalowych.</html>");
		add(authorsLabel, BorderLayout.CENTER);

		JButton okButton = new JButton("Ok");
		add(okButton, BorderLayout.SOUTH);
		// LISTENERY
		ActionListener closeListener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				dispose();

			}
		};
		okButton.addActionListener(closeListener);
	}
}
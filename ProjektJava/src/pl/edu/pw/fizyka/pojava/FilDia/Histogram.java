package pl.edu.pw.fizyka.pojava.FilDia;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.statistics.SimpleHistogramBin;
import org.jfree.data.statistics.SimpleHistogramDataset;

/* Autor: Filip Oleszczuk
 * Klasa tworzy Histogram o 4 binach, który następnie rysuje.
 * Bin 0 mówi nam ile kulek nie przeszło żadnej przesłony.
 * Bin 1 mówi nam ile przeszło 1 przeszłonę, ale już 2 nie dało rady 2.
 * Bin 2 mówi nam ile przeszło 2, ale nie dało rady 3.
 * Bin 3 mówi nam ile przeszło 3 i dotarło do ludzika.
 * */

public class Histogram extends JPanel {

	private static final long serialVersionUID = 8170568464339271173L;
	public static SimpleHistogramDataset dataset;
	public static JFreeChart chart;

	public Histogram() {

		// create the dataset with appropriate bins and some initial data
		dataset = new SimpleHistogramDataset("Key");
		dataset.addBin(new SimpleHistogramBin(-0.5, 0.5, true, false));
		dataset.addBin(new SimpleHistogramBin(0.5, 1.5, true, false));
		dataset.addBin(new SimpleHistogramBin(1.5, 2.5, true, false));
		dataset.addBin(new SimpleHistogramBin(2.5, 3.5, true, false));
		dataset.addObservations(new double[] {});

		chart = ChartFactory.createHistogram("Histogram",
				Language.translation[36], Language.translation[35], dataset,
				PlotOrientation.VERTICAL, false, false, false);
		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setDomainZeroBaselineVisible(false);
		plot.getDomainAxis().setStandardTickUnits(
				NumberAxis.createIntegerTickUnits());
		plot.getRangeAxis().setStandardTickUnits(
				NumberAxis.createIntegerTickUnits());
		ChartPanel panel = new ChartPanel(chart);

		revalidate();
		add(panel);

	}

}
package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

/* Autor: Diana Pawłowska
 * Okno wyboru języka typu JFrame. Mamy do wyboru 2 guziki, po wybraniu któregoś tablica z tłumaczeniami
 * zostaje przekazana do tablicy public static translation[], która następnie jest rozprowadzana
 * do całej reszty programu.
 * */

public class Language extends JFrame {

	public static String translation[];
	public static int lang;

	private static final long serialVersionUID = -5228061989676163169L;

	public Language() throws HeadlessException {
		super("Choose your language");
		setBounds(800, 300, 300, 200);
		setLayout(new GridLayout(2, 0));
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		final String englishLang[] = {
				"Exit",
				"Ok",
				"Animation",
				"Fast",
				"Slow",
				"Monoenergetic",
				"Source",
				"Plutonium",
				"Uranium",
				"Radium",
				"Energy",
				"Chart",
				"File",
				"Save as",
				"Properties",
				"Parameters",
				"Concrete",
				"Lead",
				"About",
				"Help",
				"Authors",
				"Before starting our experiment you have to choose covers.",
				"No parameters has been choosen.",
				"You can not change parameters while animation is running.",
				"<html><tab>Authors:<br>Diana Pawłowska<br> Filip Oleszczuk</html>",
				"Weakening of gamma radiation", "Please choose language",
				"Error!", "Continue!", "Clear all",
				"The amount of generated particles: ",
				"The amount of particles that did not pass any cover: ",
				"The amount of particles that did pass 1 cover: ",
				"The amount of particles that did pass 2 covers: ",
				"The amount of particles that did pass 3 covers: ",
				"Particles amout", "Covers", "Save as...", "Save",
				"Plutonium 239 Concrete", "Plutonium 239 Lead",
				"Uranium 235 Concrete", "Uranium 235 Lead", "Rad 226 Concrete",
				"Rad 226 Lead", "Theoretical chart", "Data",
				"The intensity of the radiation beam", "Thickness [mm]" };

		final String polishLang[] = {
				"Wyjście",
				"Ok",
				"Animacja",
				"Szybka",
				"Wolna",
				"Monoenergetyczne",
				"Źródło promieniowania",
				"Pluton",
				"Uran",
				"Rad",
				"Energia promieniowania",
				"Wykres",
				"Plik",
				"Zapisz",
				"Narzędzia",
				"Zmien parametry",
				"Beton",
				"Ołów",
				"O programie",
				"Pomoc",
				"Autorzy",
				"Zanim rozpoczniesz eksperyment wybierz przesłony",
				"Żadne parametry nie zostały wybrane",
				"Nie możesz zmienić parametrów, podczas działania programu",
				"<html><tab>Autorami programu są <br>Diana Pawłowska<br> Filip Oleszczuk</html>",
				"Osłabienie promieniowania gamma", "Proszę wybierz język",
				"Błąd!", "Kontynuuj!", "Wyczyść wszystko",
				"Ilosc wygenerowanych czasteczek: ",
				"Ilosc czasteczek, które nie przeszły żadnej przesłony: ",
				"Ilosc czasteczek, które przeszły 1 przesłone: ",
				"Ilosc czasteczek, które przeszły 2 przesłone: ",
				"Ilosc czasteczek, które przeszły 3 przesłone: ",
				"Ilość cząsteczek", "Przesłony", "Zapisz jako...", "Zapisz",
				"Pluton 239 Beton", "Pluton 239 Ołów", "Uran 235 Beton",
				"Uran 235 Ołów", "Rad 226 Beton", "Rad 226 Ołów",
				"Wykres teoretyczny", "Dane",
				"Natężenie wiązki promieniowania", "Grubość [mm]", };

		JButton polishButton = new JButton("Polish");
		JButton englishButton = new JButton("English");
		add(polishButton);
		add(englishButton);

		ActionListener polishButtonListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				translation = polishLang;
				lang = 0;
				dispose();
				Main mainFrame = new Main();
				mainFrame.setVisible(true);

			}
		};

		ActionListener englishButtonListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				translation = englishLang;
				lang = 1;
				dispose();
				Main mainFrame = new Main();
				mainFrame.setVisible(true);

			}
		};

		polishButton.addActionListener(polishButtonListener);
		englishButton.addActionListener(englishButtonListener);
	}
}

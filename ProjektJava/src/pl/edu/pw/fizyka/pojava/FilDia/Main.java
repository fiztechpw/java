package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.border.Border;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jfree.chart.ChartUtilities;

/* Autor: Filip Oleszczuk i Diana Pawłowska
 * Klasa Main. W niej są wszystkie podstawowe oprogramowane guziki.
 *
 * */

public class Main extends JFrame {

	public static String radiationSource;
	public static int radiationEnergy = 5;
	public static int pause = 0;
	public static boolean monoenergetic = false;
	final JCheckBox monoenergeticCheckBox = new JCheckBox(
			Language.translation[5]);

	private static final long serialVersionUID = -2045592721655042123L;
	DrawingWindow drawWindow = new DrawingWindow();
	Parameters parameterWindow = new Parameters(drawWindow);

	public Main() throws HeadlessException {
		super(Language.translation[25]);
		setBounds(550, 200, 880, 550);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu fileMenu = new JMenu(Language.translation[12]);
		menuBar.add(fileMenu);
		JMenu propertiesMenu = new JMenu(Language.translation[14]);
		menuBar.add(propertiesMenu);

		JMenu aboutMenu = new JMenu(Language.translation[18]);
		menuBar.add(aboutMenu);
		JMenuItem saveOption = new JMenuItem(Language.translation[13]);
		fileMenu.add(saveOption);
		JMenuItem exitOption = new JMenuItem(Language.translation[0]);
		fileMenu.add(exitOption);
		JMenuItem changeParamOption = new JMenuItem(Language.translation[15]);
		propertiesMenu.add(changeParamOption);
		JMenuItem helpOption = new JMenuItem(Language.translation[19]);
		aboutMenu.add(helpOption);
		JMenuItem authorsOption = new JMenuItem(Language.translation[20]);
		aboutMenu.add(authorsOption);

		JPanel RightPanel = new JPanel(new GridLayout(10, 0));
		RightPanel.setVisible(true);
		JLabel chartLabel = new JLabel(Language.translation[11]);
		RightPanel.add(chartLabel);

		final JComboBox<String> chartList = new JComboBox<String>();
		chartList.setEditable(true);

		chartList.addItem(Language.translation[39]);
		chartList.addItem(Language.translation[40]);

		chartList.addItem(Language.translation[41]);
		chartList.addItem(Language.translation[42]);

		chartList.addItem(Language.translation[43]);
		chartList.addItem(Language.translation[44]);

		RightPanel.add(chartList);

		JLabel sourceLabel = new JLabel(Language.translation[6]);
		RightPanel.add(sourceLabel);
		final JComboBox<String> sourceList = new JComboBox<String>();
		sourceList.setEditable(true);
		sourceList.addItem(Language.translation[7]);
		sourceList.addItem(Language.translation[8]);
		sourceList.addItem(Language.translation[9]);
		RightPanel.add(sourceList);

		chartList.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub

				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();

					if (item.equals(Language.translation[39])
							|| item.equals(Language.translation[40]))
						Chart.fluxStart = 23*Math.pow(10, 8);

					if (item.equals(Language.translation[41])
							|| item.equals(Language.translation[42]))
						Chart.fluxStart = 71100;
					
					if (item.equals(Language.translation[43])
							|| item.equals(Language.translation[44]))
						Chart.fluxStart = 3.67 * Math.pow(10, 10);
					Chart.currentFlux = Chart.fluxStart;

					if (item.equals(Language.translation[39])) {

						Chart.currentFlux = Chart.fluxStart;
						Chart.dataSet.clear();
						for (int ii = 0; ii <= 450; ii++) {
							Chart.dataSet.add(ii, Chart.currentFlux);
							Chart.currentFlux = Chart.fluxStart
									* Math.exp(-0.0134 * ii);
						}
					}
					if (item.equals(Language.translation[40])) {

						Chart.currentFlux = Chart.fluxStart;
						Chart.dataSet.clear();
						for (int ii = 0; ii <= 100; ii += 1) {
							Chart.dataSet.add(ii, Chart.currentFlux);
							Chart.currentFlux = Chart.fluxStart
									* Math.exp(-0.072 * ii);
						}

					}
					if (item.equals(Language.translation[41])) {

						Chart.currentFlux = Chart.fluxStart;
						Chart.dataSet.clear();
						for (int ii = 0; ii <= 450; ii++) {
							Chart.dataSet.add(ii, Chart.currentFlux);
							Chart.currentFlux = Chart.fluxStart
									* Math.exp(-0.015 * ii);
						}

					}
					if (item.equals(Language.translation[42])) {

						Chart.currentFlux = Chart.fluxStart;
						Chart.dataSet.clear();
						for (int ii = 0; ii <= 80; ii += 1) {
							Chart.dataSet.add(ii, Chart.currentFlux);
							Chart.currentFlux = Chart.fluxStart
									* Math.exp(-0.086 * ii);
						}

					}
					if (item.equals(Language.translation[43])) {

						Chart.currentFlux = Chart.fluxStart;
						Chart.dataSet.clear();
						for (int ii = 0; ii <= 450; ii++) {
							Chart.dataSet.add(ii, Chart.currentFlux);
							Chart.currentFlux = Chart.fluxStart
									* Math.exp(-0.0109 * ii);
						}

					}
					if (item.equals(Language.translation[44])) {

						Chart.currentFlux = Chart.fluxStart;
						Chart.dataSet.clear();
						for (int ii = 0; ii <= 125; ii += 1) {
							Chart.dataSet.add(ii, Chart.currentFlux);
							Chart.currentFlux = Chart.fluxStart
									* Math.exp(-0.054 * ii);
						}

					}

				}
			}
		});

		sourceList.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals(Language.translation[7])) {
						radiationSource = Language.translation[7];

					}
					if (item.equals(Language.translation[8])) {
						radiationSource = Language.translation[8];

					}
					if (item.equals(Language.translation[9])) {
						radiationSource = Language.translation[9];

					}

				}

			}
		});

		JLabel energyLabel = new JLabel(Language.translation[10]);
		RightPanel.add(energyLabel);

		final JComboBox<String> energyList = new JComboBox<String>();
		energyList.setEditable(true);
		energyList.addItem("5MeV");
		energyList.addItem("10MeV");
		energyList.addItem("15MeV");
		energyList.addItem("20MeV");
		RightPanel.add(energyList);

		energyList.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals("5MeV")) {
						radiationEnergy = 5;
					}
					if (item.equals("10MeV")) {
						radiationEnergy = 10;
					}
					if (item.equals("15MeV")) {
						radiationEnergy = 15;
					}
					if (item.equals("20MeV")) {
						radiationEnergy = 20;
					}

				}

			}
		});

		JLabel animationLabel = new JLabel(Language.translation[2]);
		RightPanel.add(animationLabel);

		ButtonGroup animationButtonGroup = new ButtonGroup();
		JRadioButton fastButton = new JRadioButton(Language.translation[3]);
		JRadioButton slowButton = new JRadioButton(Language.translation[4]);
		animationButtonGroup.add(fastButton);
		animationButtonGroup.add(slowButton);
		RightPanel.add(fastButton);
		RightPanel.add(slowButton);
		fastButton.setSelected(true);

		fastButton.addChangeListener(animationButtonGroupListener);
		slowButton.addChangeListener(animationButtonGroup1Listener);

		RightPanel.add(monoenergeticCheckBox);

		Border loweredbevel = BorderFactory.createLoweredBevelBorder();
		RightPanel.setBorder(loweredbevel);
		add(RightPanel, BorderLayout.EAST);

		final Calculation thread = new Calculation(drawWindow);

		JTabbedPane tabsPane = new JTabbedPane();
		add(tabsPane, BorderLayout.CENTER);
		JPanel tab1Panel = drawWindow;
		Chart chartWindow = new Chart();
		JPanel tab2Panel = chartWindow;

		Histogram histogramWindow = new Histogram();
		JPanel tab3Panel = histogramWindow;

		tabsPane.addTab(Language.translation[2], tab1Panel);
		tabsPane.addTab(Language.translation[11], tab2Panel);
		tabsPane.addTab("Histogram", tab3Panel);
		tabsPane.setEnabledAt(2, true);

		JPanel SouthPanel = new JPanel(new FlowLayout());
		SouthPanel.setVisible(true);

		final JButton clearAllButton = new JButton(Language.translation[29]);

		ActionListener clearAllButtonListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (pause == 1) {
					Parameters.isChoosenSomething = 0;
					Parameters.numberOfCovers = 0;
					Parameters.whatCovers[0] = 0;
					Parameters.whatCovers[1] = 0;
					Parameters.whatCovers[2] = 0;
					drawWindow.repaint();

					DrawingWindow.fillBalls();
					Calculation.movingBalls = 1;
					Calculation.n = 0;
					Calculation.n1 = 0;
					Calculation.n2 = 0;
					Calculation.n3 = 0;
					Calculation.N = 0;
					energyList.setSelectedIndex(0);
					sourceList.setSelectedIndex(0);
					monoenergeticCheckBox.setSelected(false);
					Parameters.howManyCovers.clearSelection();
					Parameters.whatKindOfCoverComboBox1.setEnabled(true);
					Parameters.howManyCmCover1.setEnabled(true);
					Parameters.whatKindOfCoverComboBox2.setEnabled(true);
					Parameters.howManyCmCover2.setEnabled(true);

					Parameters.howManyCmCover.setSelectedIndex(0);
					Parameters.howManyCmCover1.setSelectedIndex(0);
					Parameters.howManyCmCover2.setSelectedIndex(0);

					Parameters.whatKindOfCoverComboBox.setSelectedIndex(0);
					Parameters.whatKindOfCoverComboBox1.setSelectedIndex(0);
					Parameters.whatKindOfCoverComboBox2.setSelectedIndex(0);

					Calculation.temp = 0;
					Calculation.temp1 = 0;
					Calculation.temp2 = 0;
					Calculation.temp3 = 0;
					Histogram.dataset.clearObservations();
					chartList.setSelectedIndex(0);

				}

			}
		};

		clearAllButton.addActionListener(clearAllButtonListener);
		clearAllButton.setEnabled(false);
		final JButton startButton = new JButton("Start!");
		SouthPanel.add(startButton);

		ActionListener startButtonListener = new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				if (Parameters.isChoosenSomething == 1
						&& Calculation.doAnimation == false) {
					startButton.setEnabled(false);
					sourceList.setEnabled(false);
					energyList.setEnabled(false);
					Calculation.doAnimation = true;
					monoenergeticCheckBox.setEnabled(false);
					clearAllButton.setEnabled(false);

					thread.start();
				}
				if (Parameters.isChoosenSomething == 0) {
					JOptionPane.showMessageDialog(Main.this,
							Language.translation[21], Language.translation[22],
							JOptionPane.ERROR_MESSAGE);
				}
				if (Calculation.doAnimation == true
						&& Parameters.isChoosenSomething == 1) {
					pause = 0;
					startButton.setEnabled(false);
					sourceList.setEnabled(false);
					energyList.setEnabled(false);
					monoenergeticCheckBox.setEnabled(false);
					clearAllButton.setEnabled(false);
					thread.resume();

				}

			}
		};
		startButton.addActionListener(startButtonListener);

		ActionListener pauseButtonListener = new ActionListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent e) {
				startButton.setEnabled(true);
				sourceList.setEnabled(true);
				energyList.setEnabled(true);
				clearAllButton.setEnabled(true);
				monoenergeticCheckBox.setEnabled(true);
				pause = 1;
				thread.suspend();

			}
		};

		ActionListener saveListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle(Language.translation[38]);

				int result = chooser.showDialog(null, Language.translation[37]);
				File currentFile;
				if (JFileChooser.APPROVE_OPTION == result) {

					if (result != JFileChooser.APPROVE_OPTION) {
						return;
					}

					try {
						currentFile = chooser.getSelectedFile();
						String fileName = chooser.getSelectedFile()
								.getAbsolutePath();
						String fileName1 = chooser.getSelectedFile()
								.getAbsolutePath();
						if (!fileName.endsWith(".txt")) {
							fileName += ".txt";
							currentFile = new File(fileName);
						}
						BufferedWriter bufferedWriter = new BufferedWriter(
								new OutputStreamWriter(new FileOutputStream(
										currentFile), Charset.forName("UTF-8")));

						bufferedWriter.write(Language.translation[30]
								+ Calculation.N);
						bufferedWriter.newLine();
						bufferedWriter.write(Language.translation[31]
								+ Calculation.n);
						bufferedWriter.newLine();
						bufferedWriter.write(Language.translation[32]
								+ Calculation.n1);
						bufferedWriter.newLine();
						bufferedWriter.write(Language.translation[33]
								+ Calculation.n2);
						bufferedWriter.newLine();
						bufferedWriter.write(Language.translation[34]
								+ Calculation.n3);
						ChartUtilities.saveChartAsJPEG(new File(fileName1
								+ "Histogram.jpg"), Histogram.chart, 800, 600);
						ChartUtilities.saveChartAsJPEG(new File(fileName1
								+ "Chart.jpg"), Chart.lineGraph, 800, 600);

						bufferedWriter.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}

				}

			}
		};
		saveOption.addActionListener(saveListener);

		JButton pauseButton = new JButton("Pause!");
		SouthPanel.add(pauseButton);
		SouthPanel.add(clearAllButton);

		pauseButton.addActionListener(pauseButtonListener);

		add(SouthPanel, BorderLayout.SOUTH);

		// LISTENERY
		ActionListener closeListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		};
		exitOption.addActionListener(closeListener);

		ActionListener autorzy = new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Authors authorsWindow = new Authors();
				authorsWindow.setVisible(true);

			}
		};
		authorsOption.addActionListener(autorzy);

		ActionListener changeParamListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (Calculation.doAnimation == false || pause == 1) {
					parameterWindow.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(Main.this,
							Language.translation[23]);
				}

			}
		};
		changeParamOption.addActionListener(changeParamListener);

		ActionListener helpWindowListener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (Language.lang == 0) {
					HelpPL helpWindow = new HelpPL();
					helpWindow.setVisible(true);
				}
				if (Language.lang == 1) {
					HelpENG helpWindow = new HelpENG();
					helpWindow.setVisible(true);
				}

			}
		};

		helpOption.addActionListener(helpWindowListener);
		monoenergeticCheckBox.addItemListener(monoenergeticListener);

	}

	ChangeListener animationButtonGroupListener = new ChangeListener() {
		public void stateChanged(ChangeEvent changEvent) {
			AbstractButton aButton = (AbstractButton) changEvent.getSource();
			ButtonModel aModel = aButton.getModel();
			boolean pressed = aModel.isPressed();
			if (pressed == true) {
				Calculation.animationSpeed = 10;

			}

		}
	};

	ChangeListener animationButtonGroup1Listener = new ChangeListener() {
		public void stateChanged(ChangeEvent changEvent) {
			AbstractButton aButton = (AbstractButton) changEvent.getSource();
			ButtonModel aModel = aButton.getModel();
			boolean pressed = aModel.isPressed();
			if (pressed == true) {
				Calculation.animationSpeed = 50;
			}

		}
	};

	ItemListener monoenergeticListener = new ItemListener() {

		@Override
		public void itemStateChanged(ItemEvent e) {
			if (monoenergeticCheckBox.isSelected() == true) {
				monoenergetic = true;

			} else
				monoenergetic = false;

		}
	};

	public static void main(String[] args) {
		Language frame = new Language();
		frame.setVisible(true);

	}

}

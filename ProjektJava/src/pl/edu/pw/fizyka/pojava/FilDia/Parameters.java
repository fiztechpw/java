package pl.edu.pw.fizyka.pojava.FilDia;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/* Autor: Diana Pawłowska
 * Klasa ta otwiera nowe okno typu JFrame, w którym wybieramy rodzaj przesłony,
 * jej grubość oraz ilość przeszłon. Przekazujemy z Maina obiekt drawingWindowFromMain,
 * abyśmy mogli rysować nabieżąco dodane przesłony metodą repaint().
 * Klasa posiada ButtonGroupe, na której wybieramy ilość przesłon. Odpowiednie zaznaczenie
 * guzika powoduje możliwość wyboru grubości oraz rodzaju przesłony. Listenery nasłuchują zmian,
 * a następnie przekazuje wybrane parametry do tablic zadeklarowanych poniżej. Dodatkowo zmienna
 * isChoosenSomething sprawdza czy coś wybraliśmy.
 * */

public class Parameters extends JFrame {

	private static final long serialVersionUID = -4954975408511901524L;

	DrawingWindow drawingWindow;

	public static int numberOfCovers = 0;
	public static int cmCovers[] = new int[3];
	public static int whatCovers[] = new int[3];
	public static int isChoosenSomething = 0;

	public static ButtonGroup howManyCovers = new ButtonGroup();
	public static JComboBox<String> whatKindOfCoverComboBox = new JComboBox<String>();
	public static JComboBox<String> whatKindOfCoverComboBox1 = new JComboBox<String>();
	public static JComboBox<String> whatKindOfCoverComboBox2 = new JComboBox<String>();
	public static JComboBox<String> howManyCmCover1 = new JComboBox<String>();
	public static JComboBox<String> howManyCmCover2 = new JComboBox<String>();
	public static JComboBox<String> howManyCmCover = new JComboBox<String>();

	public Parameters(DrawingWindow drawingWindowFromMain)
			throws HeadlessException {
		super(Language.translation[14]);
		drawingWindow = drawingWindowFromMain;
		setLayout(new BorderLayout());
		setBounds(800, 300, 400, 150);
		cmCovers[0] = 5;
		cmCovers[1] = 5;
		cmCovers[2] = 5;
		whatCovers[0] = 0;
		whatCovers[1] = 0;
		whatCovers[2] = 0;
		setResizable(false);

		JPanel southPanel = new JPanel(new FlowLayout());
		southPanel.setVisible(true);

		JButton Ok = new JButton("Ok");
		southPanel.add(Ok);

		add(southPanel, BorderLayout.SOUTH);

		JPanel centerPanel = new JPanel(new GridLayout(3, 3));
		centerPanel.setVisible(true);

		JRadioButton just1Cover = new JRadioButton("1");
		JRadioButton just2Cover = new JRadioButton("2");
		JRadioButton just3Cover = new JRadioButton("3");
		howManyCovers.add(just1Cover);
		howManyCovers.add(just2Cover);
		howManyCovers.add(just3Cover);
		centerPanel.add(just1Cover);
		centerPanel.add(just2Cover);
		centerPanel.add(just3Cover);

		whatKindOfCoverComboBox.setEditable(true);
		whatKindOfCoverComboBox.addItem(Language.translation[16]);
		whatKindOfCoverComboBox.addItem(Language.translation[17]);
		centerPanel.add(whatKindOfCoverComboBox);

		whatKindOfCoverComboBox1.setEditable(true);
		whatKindOfCoverComboBox1.addItem(Language.translation[16]);
		whatKindOfCoverComboBox1.addItem(Language.translation[17]);
		centerPanel.add(whatKindOfCoverComboBox1);

		whatKindOfCoverComboBox2.setEditable(true);
		whatKindOfCoverComboBox2.addItem(Language.translation[16]);
		whatKindOfCoverComboBox2.addItem(Language.translation[17]);
		centerPanel.add(whatKindOfCoverComboBox2);

		howManyCmCover.setEditable(true);
		howManyCmCover.addItem("5cm");
		howManyCmCover.addItem("10cm");
		howManyCmCover.addItem("15cm");
		howManyCmCover.addItem("20cm");
		howManyCmCover.addItem("25cm");
		howManyCmCover.addItem("30cm");
		centerPanel.add(howManyCmCover);

		howManyCmCover1.setEditable(true);
		howManyCmCover1.addItem("5cm");
		howManyCmCover1.addItem("10cm");
		howManyCmCover1.addItem("15cm");
		howManyCmCover1.addItem("20cm");
		howManyCmCover1.addItem("25cm");
		howManyCmCover1.addItem("30cm");
		centerPanel.add(howManyCmCover1);

		howManyCmCover2.setEditable(true);
		howManyCmCover2.addItem("5cm");
		howManyCmCover2.addItem("10cm");
		howManyCmCover2.addItem("15cm");
		howManyCmCover2.addItem("20cm");
		howManyCmCover2.addItem("25cm");
		howManyCmCover2.addItem("30cm");
		centerPanel.add(howManyCmCover2);

		add(centerPanel, BorderLayout.CENTER);

		ActionListener closeListener = new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				dispose();
			}
		};
		Ok.addActionListener(closeListener);

		ChangeListener just1CoverListener = new ChangeListener() {
			public void stateChanged(ChangeEvent changEvent) {
				AbstractButton aButton = (AbstractButton) changEvent
						.getSource();
				ButtonModel aModel = aButton.getModel();
				boolean pressed = aModel.isPressed();
				if (pressed == true) {
					isChoosenSomething = 1;
					numberOfCovers = 1;
					whatKindOfCoverComboBox1.setEnabled(false);
					whatKindOfCoverComboBox2.setEnabled(false);
					howManyCmCover1.setEnabled(false);
					howManyCmCover2.setEnabled(false);
					drawingWindow.repaint();

				}

			}
		};

		ChangeListener just2CoverListener = new ChangeListener() {
			public void stateChanged(ChangeEvent changEvent) {
				AbstractButton aButton = (AbstractButton) changEvent
						.getSource();
				ButtonModel aModel = aButton.getModel();
				boolean pressed = aModel.isPressed();
				if (pressed == true) {
					isChoosenSomething = 1;
					numberOfCovers = 2;
					whatKindOfCoverComboBox1.setEnabled(true);
					howManyCmCover1.setEnabled(true);
					whatKindOfCoverComboBox2.setEnabled(false);
					howManyCmCover2.setEnabled(false);
					drawingWindow.repaint();
				}

			}
		};

		ChangeListener just3CoverListener = new ChangeListener() {
			public void stateChanged(ChangeEvent changEvent) {
				AbstractButton aButton = (AbstractButton) changEvent
						.getSource();
				ButtonModel aModel = aButton.getModel();
				boolean pressed = aModel.isPressed();
				if (pressed == true) {
					isChoosenSomething = 1;
					numberOfCovers = 3;
					whatKindOfCoverComboBox1.setEnabled(true);
					howManyCmCover1.setEnabled(true);
					whatKindOfCoverComboBox2.setEnabled(true);
					howManyCmCover2.setEnabled(true);
					drawingWindow.repaint();
				}

			}
		};

		just1Cover.addChangeListener(just1CoverListener);
		just2Cover.addChangeListener(just2CoverListener);
		just3Cover.addChangeListener(just3CoverListener);

		howManyCmCover.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals("5cm")) {
						cmCovers[0] = 5;
						drawingWindow.repaint();
					}
					if (item.equals("10cm")) {
						cmCovers[0] = 10;
						drawingWindow.repaint();
					}
					if (item.equals("15cm")) {
						cmCovers[0] = 15;
						drawingWindow.repaint();
					}
					if (item.equals("20cm")) {
						cmCovers[0] = 20;
						drawingWindow.repaint();
					}

					if (item.equals("25cm")) {
						cmCovers[0] = 25;
						drawingWindow.repaint();
					}

					if (item.equals("30cm")) {
						cmCovers[0] = 30;
						drawingWindow.repaint();
					}

				}

			}
		});

		howManyCmCover1.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals("5cm")) {
						cmCovers[1] = 5;
						drawingWindow.repaint();
					}
					if (item.equals("10cm")) {
						cmCovers[1] = 10;
						drawingWindow.repaint();
					}
					if (item.equals("15cm")) {
						cmCovers[1] = 15;
						drawingWindow.repaint();
					}
					if (item.equals("20cm")) {
						cmCovers[1] = 20;
						drawingWindow.repaint();
					}

					if (item.equals("25cm")) {
						cmCovers[1] = 25;
						drawingWindow.repaint();
					}

					if (item.equals("30cm")) {
						cmCovers[1] = 30;
						drawingWindow.repaint();
					}

				}

			}
		});

		howManyCmCover2.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals("5cm")) {
						cmCovers[2] = 5;
						drawingWindow.repaint();
					}
					if (item.equals("10cm")) {
						cmCovers[2] = 10;
						drawingWindow.repaint();
					}
					if (item.equals("15cm")) {
						cmCovers[2] = 15;
						drawingWindow.repaint();
					}
					if (item.equals("20cm")) {
						cmCovers[2] = 20;
						drawingWindow.repaint();
					}

					if (item.equals("25cm")) {
						cmCovers[2] = 25;
						drawingWindow.repaint();
					}

					if (item.equals("30cm")) {
						cmCovers[2] = 30;
						drawingWindow.repaint();
					}
				}

			}
		});

		whatKindOfCoverComboBox.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals(Language.translation[16])) {
						whatCovers[0] = 0;
						drawingWindow.repaint();
					}
					if (item.equals(Language.translation[17])) {
						whatCovers[0] = 1;
						drawingWindow.repaint();
					}

				}
			}

		});
		whatKindOfCoverComboBox1.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals(Language.translation[16])) {
						whatCovers[1] = 0;
						drawingWindow.repaint();
					}
					if (item.equals(Language.translation[17])) {
						whatCovers[1] = 1;
						drawingWindow.repaint();
					}

				}
			}

		});
		whatKindOfCoverComboBox2.addItemListener(new ItemListener() {

			@Override
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					Object item = e.getItem();
					if (item.equals(Language.translation[16])) {
						whatCovers[2] = 0;
						drawingWindow.repaint();
					}
					if (item.equals(Language.translation[17])) {
						whatCovers[2] = 1;
						drawingWindow.repaint();
					}

				}
			}

		});

	}

}
